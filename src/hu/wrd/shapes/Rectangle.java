package hu.wrd.shapes;

public class Rectangle implements Shape {
    private final int x;
    private final int y;

    Rectangle(int x, int y) throws Exception {
        if (x <= 0 || y <= 0) {
            throw new Exception("Invalid paramater(s). Either x and y cannot be 0 or less.");
        }

        this.x = x;
        this.y = y;
    }

    @Override
    public double getArea() {
        return x * y;
    }

    @Override
    public boolean isSame(Shape other) {
        if (Shape.super.isSame(other)) {
            return true;
        }

        if (other instanceof Rectangle) {
            return this.getArea() == other.getArea();
        }

        if (this.isSquarelike() && other instanceof Square) {
            Square square = (Square) other;
            return this.x == square.getX();
        }

        return false;
    }

    public boolean isSquarelike() {
        return this.x == this.y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return "Rectangle{" + "x=" + x + ", y=" + y + ", area=" + getArea() + '}';
    }
}
