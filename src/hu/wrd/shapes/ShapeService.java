package hu.wrd.shapes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ShapeService {
    private final List<Shape> shapes;

    public ShapeService() {
        shapes = new ArrayList<>();
    }

    public void addShapes(Shape... shapes) {
        List<Shape> shapesToAdd = new ArrayList<>();

        for (Shape shape : shapes) {
            if (contains(shape)) {
                return;
            } else {
                shapesToAdd.add(shape);
            }
        }

        this.shapes.addAll(shapesToAdd);
    }

    public void printShapesOrderByAreaAsc() {
        List<Shape> sortedShapes = new ArrayList(shapes);
        sortedShapes.sort(new ShapeComparator());

        sortedShapes.forEach(System.out::println);
        System.out.println("printShapesOrderByAreaAsc() called");
    }

    public void printShapesOrderByAreaDesc() {
        List<Shape> sortedShapes = new ArrayList(shapes);
        sortedShapes.sort(new ShapeComparator().reversed());

        sortedShapes.forEach(System.out::println);
        System.out.println("printShapesOrderByAreaDesc() called");
    }

    private boolean contains(Shape newShape) {
        return shapes.stream().anyMatch(shape -> shape.isSame(newShape));
    }
}
