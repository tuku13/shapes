package hu.wrd.shapes;

public interface Shape {
    double getArea();
    default boolean isSame(Shape other) {
        return this == other;
    }
}
