package hu.wrd.shapes;

public class Square implements Shape {
    private final int x;

    Square(int x) throws Exception {
        if (x <= 0) {
            throw new Exception("Invalid parameter, The x cannot be 0 or less.");
        }
        this.x = x;
    }

    @Override
    public boolean isSame(Shape other) {
        if (Shape.super.isSame(other)) {
            return true;
        }

        if (other instanceof Square) {
            return this.getArea() == other.getArea();
        }

        if (other instanceof Rectangle) {
            Rectangle rectangle = (Rectangle) other;
            return rectangle.isSquarelike() &&
                    this.getArea() == rectangle.getArea();
        }

        return false;
    }

    @Override
    public double getArea() {
        return x * x;
    }

    public int getX() {
        return x;
    }

    @Override
    public String toString() {
        return "Square{" + "x=" + x + ", area=" + getArea() + '}';
    }
}
