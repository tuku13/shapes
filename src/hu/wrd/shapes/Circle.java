package hu.wrd.shapes;

public class Circle implements Shape {
    private final int radius;

    Circle(int radius) throws Exception {
        if (radius <= 0) {
            throw new Exception("Inalid parameter. The radius can't be less than or equal to zero.");
        }

        this.radius = radius;
    }

    @Override
    public double getArea() {
        return radius * radius * Math.PI;
    }

    @Override
    public boolean isSame(Shape other) {
        if (Shape.super.isSame(other)) {
            return true;
        }

        if (other instanceof Circle) {
            return this.getArea() == other.getArea();
        }

        return false;
    }

    @Override
    public String toString() {
        return "Circle{" + "radius=" + radius + ", area=" + getArea() + '}';
    }
}
